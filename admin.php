<?php

include ('db.php');
$today_date =  date('d/m/y');
$d=strtotime("tomorrow");
$tomorrow_date = date('d/m/y', $d);
$e=strtotime("yesterday");
$yesterday_date = date('d/m/y', $e);
   

   
$sql = "SELECT * FROM `bookings` WHERE `room_number` NOT LIKE '%room%' and `date_d` = '$today_date'";
$result = $conn->query($sql);

?> 

<html>
<head>

<link rel="stylesheet" type="text/css" href="mainstyles.css">
<script src="jquery-2.1.4.min.js"></script>
      <link rel="stylesheet" href="bootstrap-3.3.5-dist/css/bootstrap.min.css">
      <link rel="stylesheet" href="bootstrap-3.3.5-dist/css/bootstrap-theme.min.css">
      <script src="bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
</head>
<title>CES Admin DashBoard</title>
<body>
<div class="menu_bar">
<div id="ces_logo">
<img src="ces_logo.gif" alt="" height="50" width="100"/>
</div>
<div id="heading">
<strong>CES ADMIN DASHBOARD</strong>
</div>
<div id="date_and_time">
<div id="date">
</div>
<div id="time">
</div>
</div>
</div>
<div class="main_content">
<div class="admin_date_main">
<div class="manage_app_main">
<a href ="" id = "manage_app" data-toggle="modal" data-target="#myModal">Manage App</a>
</div>
<div class="admin_date_selection_text">
Please select a date
</div>
<div class="admin_date_selection_dropdown">
<select class="admin_date_selection_dropdown_class">
 <?php  
   echo '<option value='.$yesterday_date.'>'.$yesterday_date.'</option>';
   echo '<option value='.$today_date.' selected="selected">'.$today_date.'</option>';
   echo '<option value='.$tomorrow_date.'>'.$tomorrow_date.'</option>'; ?>
  </select> 
 </div>
 <div class="admin_button_main">
 <button type="button" id="go">Check Bookings</button>
 </div>
 </div>
<div id="main_content_heading">Bookings</div>
<hr>
<div class="booking_records">
<?php
while($row = $result->fetch_assoc()) {
        echo "<div id=booking_division".$row["id"]." class='booking_divisions'>";
        echo "<div id=booking".$row["id"]." class='bookings'>";
        echo "<span id='field1'>Resource ID : " . $row["id"]."</span>";
		echo "<span id='field2'>Resource Name : " . $row["room_number"]."</span>";
		echo "<span id='field3'>Booking Name : " . $row["booking_name"]."</span>";
		echo "<span id='field4'>Booking Timings : " . $row["booking_timings"]."</span>";
		echo "</div>";
		echo "<div class='field5'><button type ='button' id='".$row["id"]."' class='approve'>APPROVE</button><button type ='button' id='recieve".$row["id"]."' class='recieve' disabled='disabled'>RECIEVE</button>"."</div>" ;
		echo "</div>";
    }
?>
</div>
</div>


<!-- Modal -->
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title"><b>Manage Your Portal Application</b></h4>
      </div>
	  
      <div class="modal-body">
	  <form id="room_form" method="post" action ="manage_app.php" autocomplete="off" enctype="multipart/form-data">
     <p id="form_field_one"><label for="rnameid">Enter Room ID :</label><input type="text" name="rnameid" id="rnameid" placeholder="room6" ></p> 
	 <p id="form_field_two"><label for="rname">Enter Room Name :</label><input type="text" name="rname" id="rname" placeholder="Conference Hall" ></p> 
	  <p id="form_field_three"><label for="upload">Upload Carousel Images (4) :</label><input type="file" multiple="multiple" name="fileToUpload[]" id="fileToUpload"></p>
	       <div class="modal-footer">
	    <input type="submit" class="btn btn-default" value="Submit" id="submit"/>
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
     </form>
      </div>
 
    </div>

  </div>
</div>

 

</body>
<script>
$(document).ready(function(){

// $('body').on('click', '#submit', function(){

 // var xmlhttp = new XMLHttpRequest();
        // xmlhttp.onreadystatechange = function() {
            // if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {		
              /* document.getElementById("txtHint").innerHTML = xmlhttp.responseText; */
             // alert(xmlhttp.responseText);
            // }
        // }
        // xmlhttp.open("GET", "manage_app.php", true);
        // xmlhttp.send();


// });


setInterval(function(){
var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {		
				var output = xmlhttp.responseText;
				//alert(output);
				var res = output.split("||");
				var idarr = res[0].split(",");
				idarr = idarr.slice(0, -1);
				var dsarr = res[1].split(",");				
				dsarr = dsarr.slice(0, -1);
				var idar = $.makeArray( idarr );
				var dsar = $.makeArray( dsarr );
				
				for(i=0;i<idar.length;i++){
				
				var idar_id = '#'+idar[i];
				var dsar_id = '#recieve'+idar[i];
				
				  if(dsar[i]=="RETURNED DEVICE"){
				$(idar_id).attr('disabled',true);
				$(dsar_id).attr('disabled',true);
				}
                  if(dsar[i]=="APPROVAL PENDING"){
				$(idar_id).removeAttr('disabled',true);
				}
                  if(dsar[i]=="APPROVED DEVICE"){
				$(idar_id).attr('disabled',true);
				$(dsar_id).removeAttr('disabled',true);
				}
                }				

        }
		}
        xmlhttp.open("GET", "ajax_check.php", true);
        xmlhttp.send();
},10000000000000);






setInterval(function(){
var dNow = new Date();
var month = ["January","February","March","	April","May","June","July","August","September","October","November","December"]
var localdate= dNow.getDate()  + ' ' + month[dNow.getMonth()] + ' ' + dNow.getFullYear();
var localtime=  dNow.getHours() + ':' + dNow.getMinutes();
var minutes = dNow.getMinutes();
if (minutes < 10){
minutes = '0'+dNow.getMinutes();
localtime=  dNow.getHours() + ':' + minutes;
}

$('#date').text(localdate);
$('#time').text(localtime);
},500);

});



$('body').on('click', '.approve', function(){
var delete_id = this.id;
var delete_button = "#"+ delete_id;
var recieve_button = "#recieve"+delete_id;
 var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {		
              //document.getElementById("txtHint").innerHTML = xmlhttp.responseText;

			 $(delete_button).attr('disabled',true);
			 
			 $(recieve_button).removeAttr('disabled',false);
            }
        }
        xmlhttp.open("GET", "approval.php?delete_id=" + delete_id, true);
        xmlhttp.send();


});


$('body').on('click', '.recieve', function(){
$(this).attr('disabled',true);
var recieve_id = this.id;
var res = recieve_id.slice(7);
var res_id = "#booking_division"+res;
 var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {		
              //document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
			  console.log(res_id);
			  $(res_id).attr('disabled',true);
            }
        }
        xmlhttp.open("GET", "recieve.php?res=" + res, true);
        xmlhttp.send();

});



$("#go").click(function(){

var date = $(".admin_date_selection_dropdown_Class").val();
 var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
			$('.booking_records').empty();
			$('.booking_records').append(xmlhttp.responseText);
              //document.getElementById("txtHint").innerHTML = xmlhttp.responseText;

            }
        }
        xmlhttp.open("GET", "check_bookings.php?date=" + date, true);
        xmlhttp.send();
});

</script>
</html>