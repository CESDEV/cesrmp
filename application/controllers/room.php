<?php
class Room extends CI_Controller {
		

        public function __construct()
        {
                parent::__construct();
                $this->load->model('room_model');
                $this->load->helper('url_helper');
        }
		 public function index()
        {
			
			if(isset($_REQUEST['r'])){
				$current_room_number = $_REQUEST['r'];
				$current_room_date = $_REQUEST['date'];
				$data['current_status'] = $this->room_model->get_room_initial($current_room_number,$current_room_date);
				$data['room_id'] = $_REQUEST['r'];
				$this->load->view('room',$data);
			
			}else{
			$this->load->view('templates/main_content');
			}
			
			//$this->load->view('templates/footer');
        
        }
		 public function test()
        {
			$current_room_number = $_REQUEST['r'];
            $current_room_date = $_REQUEST['date'];
				$current_status = $this->room_model->get_room_initial($current_room_number,$current_room_date);
						$array = json_decode(json_encode($current_status), true);
                        $i=0;
						$addTime = "23:30";
						while($i<=48){
						$endTime = strtotime("+30 minutes", strtotime($addTime));
						$endTime = date('H:i', $endTime); 
						$arr[] =  $array[0][$endTime]; 
						$addTime = $endTime;
						$i++;
						if($i==48){
						break;
						}
						}
					    echo json_encode($arr);
							  
        }
        
}
?>