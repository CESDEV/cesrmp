<?php
include ('E:\XAMMP\htdocs\cesrmp\ajax\db.php'); 
?>
<html>
        <head>
                <title>Conference Room CES</title>
				<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>/assets/css/mainstyles.css">
				<link href='https://fonts.googleapis.com/css?family=Patua+One|Scada:400,400italic' rel='stylesheet' type='text/css'>

				<script src="<?php echo base_url();?>/assets/script/jquery-2.1.4.min.js"></script>
				<script src="<?php echo base_url();?>/assets/script/confroom_main.js"></script>
			<link rel="stylesheet" href="<?php echo base_url();?>/bootstrap-3.3.5-dist/css/bootstrap.min.css">
            <link rel="stylesheet" href="<?php echo base_url();?>/bootstrap-3.3.5-dist/css/bootstrap-theme.min.css">
            <script src="<?php echo base_url();?>/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>	
			  <script src="<?php echo base_url();?>assets/script/moment.js"></script>
        </head>
		
        <body>
		
		<div class="home_page_wrapper">

		<div class="main_content">
		<div class="left_contentDiv">
		
		<img src = "<?php echo base_url();?>/assets/css/ces_main_logo.jpg" alt= "ces_logo" id="ces_main_logo" />
		</div>
		<div class="right_contentDiv">
		<p class="heading_content">Welcome To CES Portal !</p>
		<div class="content_left">
			<div class="btnsWrap">
<button class = "use_a_room" data-toggle="modal" data-target="#roomModal">Book a Room<span class="arrow2"></span></button>
				
				
				
			</div>
			
			</div>
			<div class="content_right">
			<div class="btnsWrap2">
			<button class = "use_a_device" data-toggle="modal" data-target="#devices">Book a Device<span class="arrow2"></span></button>
			
			</div>
			</div>
			</div>
</div>

			<div class = "footer">

					<p>CES IT Pvt. Ltd., 
			1st Floor, West wing Beta Block, SSPDL,

			Old No. 25, Rajiv Gandhi Salai (OMR), Navalur, Chennai – 603103</p>

			<p>Ph: 044- 45114302 | Cell: +91 9600269429 | Fax: 044- 45114305</p> 

			<p>www.cesltd.com</p> 
				  
			</div>
            </div>
			<div id="devices" class="modal fade" role="dialog"  data-backdrop="static">
			  <div class="modal-dialog">

				<!-- Modal content-->
				<div class="modal-content">
				  <div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Please Choose Your Booking</h4>
				  </div>
				  <div class="modal-body">
				  <div class="device_date">
				  <div class="device_date_text">
				  Please select a date : 
				  </div>
				  <div class="device_date_dropdown">
				  <select class="device_date_initial_selection">
				  		<?php 
					/*   $sql2 = "SELECT * from dates_available";
					$result2 = $conn->query($sql2);
					while($row = $result2->fetch_assoc()) {
					   echo '<option value='.$row['dates_a'].'>'.$row["dates_a"].'</option>';
					  } */ 
					  $i=0;
							$today_date =  date('d/m/y');
							echo '<option value='.$today_date.'>'.$today_date.'</option>';
							while($i<=15){
							$i++;
							$tomorrow_date =  date('d/m/y', strtotime("+".$i." days"));
							echo '<option value='.$tomorrow_date.'>'.$tomorrow_date.'</option>';
							}
						   
						   
					  ?>
				  </select>
				  </div>
				  </div>
				  <hr>
					<div class = "device_images_main">
                    <?php
					$sql = "SELECt * FROM `devices_list`";
					$result = $conn->query($sql);
					while($row= $result->fetch_assoc()){
					echo '<a href="'.base_url().$row['url'].'" data-toggle="tooltip" id="'.$row['img_id'].'" class="device_images_link" title="'.$row['audience_view'].'"><img src='.$row['img_path_1'].' class="device_images" alt="" id='.$row['img_id'].' /></a>';
					}
					?>
					
					
				  </div>

				</div>

			  </div>
			</div>

</div>



<div id="roomModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Choose Your Booking</h4>
      </div>
      <div class="room_modal_body">
	  <div class="date_selection">
	  <div class="date_text">Please select a date : </div><div class="date_option"><select class="date_initial_selection">  
		
		
		<?php 
/*   $sql2 = "SELECT * from dates_available";
$result2 = $conn->query($sql2);
while($row = $result2->fetch_assoc()) { */
							$i=0;
							$today_date =  date('d/m/y');
							echo '<option value='.$today_date.'>'.$today_date.'</option>';
							while($i<=15){
							$i++;
							$tomorrow_date =  date('d/m/y', strtotime("+".$i." days"));
							echo '<option value='.$tomorrow_date.'>'.$tomorrow_date.'</option>';
							}
/*   }  */
  ?>
	  
	</select> 
</div>	
	  </div>
	  <div class="room_selection">
       <div class="room_text"> Select the room you want to book : </div><div class="room_option"> <select class="room_initial_selection">
  <?php 
  $sql = "SELECT * from rooms_list";
$result = $conn->query($sql);
while($row = $result->fetch_assoc()) {
   echo '<option value='.$row['option_value'].'>'.$row["audience_view"].'</option>';
  } 
  ?>
</select>
</div>
</div>
      </div>
      <div class="modal-footer">
	  <button type="button" id="room_initial_proceed" class="btn btn-default" >Proceed</button>
        <button type="button" id="room_initial_close" class="btn btn-default" data-dismiss="modal">Close</button>
		<input type="hidden" name="room-value" id="room-value" value="HOMEPAGE">
		<input type="hidden" name="date-value" id="date-value">
      </div>
    </div>

  </div>
</div>

</body>
					
<script>
$(document).ready(function(){


jQuery(".atributosSort option:first-child").attr("selected", true);
$("#room_initial_proceed").click(function(){
var date = $(".date_initial_selection").val();

var x;

x = $(".room_initial_selection").val();
console.log(x);
window.location = '<?php echo base_url();?>/index.php/?r='+x+'&date='+date;
});




$('.device_images_link').click(function(){
var xx = $(this).attr('href');
var date2 =  $(".device_date_initial_selection").val();

$(this).attr("href",xx+'&date='+date2);
});
});

</script>
			</html>
		
		
		
		
		     

