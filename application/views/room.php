<?php

include('E:\XAMMP\htdocs\cesrmp\ajax\db.php'); 

$sql ="select * from rooms_list";

$result = $conn->query($sql);

  while($row = $result->fetch_assoc()) {
   $path1 = $row["img_path_1"];
   $path2 = $row["img_path_2"];
   $path3 = $row["img_path_3"];
   $path4 = $row["img_path_4"];
  } 

$array = json_decode(json_encode($current_status), true);

$i=0;
$addTime = "23:30";
while($i<=48){
$endTime = strtotime("+30 minutes", strtotime($addTime));
$endTime = date('H:i', $endTime); 
$arr[] =  $array[0][$endTime]; 
$addTime = $endTime;
$i++;
if($i==48){
break;
}
}  

$rn = $array[0]['room_number'];
  //echo $rn;
  //usleep(1000000);
  
?>

<html>
   <head>
      <title>CES Resource Management Portal</title>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <script src="<?php echo base_url();?>assets/script/jquery-2.1.4.min.js"></script>
      
      <link rel="stylesheet" href="<?php echo base_url();?>/bootstrap-3.3.5-dist/css/bootstrap.min.css">
      <link rel="stylesheet" href="<?php echo base_url();?>/bootstrap-3.3.5-dist/css/bootstrap-theme.min.css">
      <script src="<?php echo base_url();?>/bootstrap-3.3.5-dist/js/bootstrap.min.js"></script>
      <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/mainstyles.css">
      <link href='https://fonts.googleapis.com/css?family=Patua+One|Scada:400,400italic' rel='stylesheet' type='text/css'>
      <script src="<?php echo base_url();?>assets/script/confroom_main.js"></script>
	  <script src="<?php echo base_url();?>assets/script/reload.js"></script>
	  <script src="<?php echo base_url();?>assets/script/moment.js"></script>
	  
   </head>
   <body onbeforeunload="return myFunction()">
      <div class="room_main_wrapper">
        
            <div class="room_header">
               <div id="room_header_logo">
                 <a href =<?php echo base_url();?> role="link">
				 <img src="<?php echo base_url();?>assets/css/ces.png" alt="CES Logo" id="ces_logo"/>
				 </a>
               </div>
              
                  <div id="room_main_heading">CES RESOURCE PORTAL</div>
                  <div id="room_dateandtime">
                     <p id="date">20th September 2015</p>
                     <p id="time"> 02 : 41 PM </p>
                  </div>
               
            </div>
            <div class="room_carousel">
			<img id="device_main_img" alt="device_img" height="320" />
               <div id="myCarousel" class="carousel slide" data-ride="carousel">
                  <!-- Indicators -->
                  <ol class="carousel-indicators">
                     <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                     <li data-target="#myCarousel" data-slide-to="1"></li>
                     <li data-target="#myCarousel" data-slide-to="2"></li>
                     <li data-target="#myCarousel" data-slide-to="3"></li>
                  </ol>
				  
                  <!-- Wrapper for slides -->
                  <div class="carousel-inner" role="listbox">
                     <div class="item active">
                        <img src="<?php echo base_url().$path1?>" alt="#" height="300">
                        <div class="carousel-caption">
                        </div>
                     </div>
                     <div class="item">
                        <img src="<?php echo base_url().$path2?>" alt="#" height="300">
                        <div class="carousel-caption">
                        </div>
                     </div>
                     <div class="item">
                        <img src="<?php echo base_url().$path3?>" alt="#" height="300">
                        <div class="carousel-caption">
                        </div>
                     </div>
                     <div class="item">
                        <img src="<?php echo base_url().$path4?>" alt="#" height="300">
                        <div class="carousel-caption">
                        </div>
                     </div>
                  </div>
                  <!-- Left and right controls -->
                  <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                  <span class="sr-only">Previous</span>
                  </a>
                  <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                  <span class="sr-only">Next</span>
                  </a>
               </div>
            </div>
            <div class="bookings_section">

			
               <div id="bookings_button">
			   
			   			               <div class="pof">
              <h3 id="pof-link">Pre Business Hours</h3>
<div class="pof_division">
 <button class="time_button" id="button000" value="00:00">00:00 AM</button>
			       <button class="time_button" id="button030" value="00:30">00:30 AM</button>
			      <button class="time_button" id="button100" value="01:00">01:00 AM</button>
				  <button class="time_button" id="button130" value="01:30">01:30 AM</button>
				  <button class="time_button" id="button200" value="02:00">02:00 AM</button>
				  <button class="time_button" id="button230" value="02:30">02:30 AM</button>
				  <button class="time_button" id="button300" value="03:00">03:00 AM</button>
				  <button class="time_button" id="button330" value="03:30">03:30 AM</button>
				  <button class="time_button" id="button400" value="04:00">04:00 AM</button>
				  <button class="time_button" id="button430" value="04:30">04:30 AM</button>
				  <button class="time_button" id="button500" value="05:00">05:00 AM</button>
				  <button class="time_button" id="button530" value="05:30">05:30 AM</button>
				  <button class="time_button" id="button600" value="06:00">01:30 PM</button>
				  <button class="time_button" id="button630" value="06:30">06:30 AM</button>
				  <button class="time_button" id="button700" value="07:00">07:00 AM</button>
				  <button class="time_button" id="button730" value="07:30">07:30 AM</button>
				  <button class="time_button" id="button800" value="08:00">08:00 PM</button>
				  <button class="time_button" id="button830" value="08:30">08:30 AM</button>
				  <button class="time_button" id="button900" value="09:00">09:00 AM</button>
				  <button class="time_button" id="button930" value="09:30">03:30 AM</button>
				  <button class="time_button" id="button1000" value="10:00">10:00 AM</button>
				  <button class="time_button" id="button1030" value="10:30">10:30 AM</button>
				  <button class="time_button" id="button1100" value="11:00">11:00 AM</button>
				  <button class="time_button" id="button1130" value="11:30">11:30 AM</button>
				  <button class="time_button" id="button1200" value="12:00">12:00 PM</button>
				  <button class="time_button" id="button1230" value="12:30">12:30 AM</button>
                  <button class="time_button" id="button1300" value="13:00">13:00 AM</button>
</div>			  
               </div>
			   
			   
			   <h3> Business Hours <a href="" role="link" id="fbh" data-toggle="modal" data-target="#myModal" class="disabled">Book Full Business Hours</a> </h3>
			   
				  <button class="time_button" id="button1330" value="13:30">13:30 PM</button>
                  <button class="time_button" id="button1400" value="14:00">02:00 PM</button>
                  <button class="time_button" id="button1430" value="14:30">02:30 PM</button>      
                  <button class="time_button" id="button1500" value="15:00">03:00 PM</button>   
                  <button class="time_button" id="button1530" value="15:30">03:30 PM</button>          
                  <button class="time_button" id="button1600" value="16:00">04:00 PM</button>       
                  <button class="time_button" id="button1630" value="16:30">04:30 PM</button>    
                  <button class="time_button" id="button1700" value="17:00">05:00 PM</button>
                  <button class="time_button" id="button1730" value="17:30">05:30 PM</button>       
                  <button class="time_button" id="button1800" value="18:00">06:00 PM</button>   
                  <button class="time_button" id="button1830" value="18:30">06:30 PM</button>           
                  <button class="time_button" id="button1900" value="19:00">07:00 PM</button>
                  <button class="time_button" id="button1930" value="19:30">07:30 PM</button>               
                  <button class="time_button" id="button2000" value="20:00">08:00 PM</button>   
                  <button class="time_button" id="button2030" value="20:30">08:30 PM</button>            
                  <button class="time_button" id="button2100" value="21:00">09:00 PM</button>            
                  <button class="time_button" id="button2130" value="21:30">09:30 PM</button>               
                  <button class="time_button" id="button2200" value="22:00">10:00 PM</button>
                 
               <div class="eof">
              <h3 id="eof-link">Extended Business Hours</h3>
<div class="eof_division">
<button class="time_button" id="button2230" value="22:30">10:30 PM</button>
<button class="time_button" id="button2300" value="23:00">11:00 PM</button>
<button class="time_button" id="button2330" value="23:30">11:30 PM</button>
</div>			  
               </div>			  
               </div>
               <div id="bookings_confirm_section">
                  <div class="book_now_section">
                     <button class="book_now" disabled="disabled" data-toggle="modal" data-target="#myModal">Book Now</button>
                  </div>
                  <ul class="list-inline">
                     <li id="booked-info"><span id ="booked"></span><span id="booked_text"> - Booked</li>
                     <li id="available-info"><span id ="available"></span><span id="available_text"> - Available</li>
					 <li id="hold-info"><span id ="hold"></span><span id="hold_text"> - Hold</li>
                  </ul>
               </div>
            </div>

         
      </div>
	  <input type="hidden" name="room-value" id="room-value" value=<?php echo $rn ?>>
	  <input type="hidden" name="room_id" id="room_id" value=<?php echo "'".$room_id."'"; ?>>
      <!-- Modal -->
      <div id="myModal" class="modal fade" role="dialog" data-backdrop="static" >
         <div class="modal-dialog">
            <!-- Modal content-->
            <div class="modal-content">
			<span class="atces">@cesltd.com</span>
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <button id="countdown_timer"></button>
               </div>
               <div class="modal-body">
			   <h4 class="modal-title">Enter Your Details</h4>
                  <span id="booking_time_heading">Booking Time : </span><span id="room_timing"></span><input type="hidden" id="room_timing_val" />
                  <div class="time_duration_radio">
				  <select class="fbh_modal_text"><option name="mins" class="radio_mins" id="fullmins" value="18" >Full Business Hours</option></select>
				  <span class="duration_select_span">
				  <select class="duration_select">
					  <option name="mins" class="radio_mins" id="30mins" value="1" >30 mins</option>		
					  <option name="mins" class="radio_mins" id="60mins" value="2" disabled="disabled">01 hour</option>		
					  <option name="mins" class="radio_mins" id="90mins" value="3" disabled="disabled">01 hour 30 mins</option>
					  <option name="mins" class="radio_mins" id="120mins" value="4" disabled="disabled">02 hours</option> </select>
					  </span>
                  </div>
                  <div class="user_details">
                     <div id="name_details">
                        <form action="" id ="user_name" autocomplete="off">
                           <p id="form_field_one"><label for="name">Enter your name :</label><input type="text" name="uname" id="uname" placeholder="John" ></p>
                           <p id="form_field_two"><label for="meeting">Purpose of resource :</label><input type="text" name="meeting" id="meeting" placeholder="Montrose client call" ></p>
						   <p id="form_field_four"><label for="email">Email id :</label><input type="email" name="email" id="useremail" placeholder="sysadmin.chn"></p>
                           <div class="buttons">
                           <button type="input" class="btn btn-default" name="submit"  data-dismiss="modal" id="submit_popup" disabled="disabled" data-toggle="modal" data-target="#popup">Book</button>
                           <button type="button" class="btn btn-default" data-dismiss="modal" id="submit_close">Close</button>
                           </div>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
	  <div id="popup" class="modal fade" role="dialog">
		<div class="dialog">
		
			<h5 id="txtHint">Success , Your booking has been Recieved!</h5>
			<div class="details1">
			<div id="conf_book_name"><span id="conf_book_label">Booking Name : </span><span id="conf_user_name"></span></div>
			<div id="conf_dura_time"><span id="conf_dura_label">Time : </span><span id="conf_user_st"></span> - <span id="conf_user_et"></span></div>
			<div id="conf_book_email"><span id="conf_email_label">E-mail Id: </span><span id="conf_user_email"></span></div>
			<div id="conf_book_password"><span id="booking_password_label">Booking Password: </span><span id="booking_password_text"></span></div>
			<p class="note_your_password"> <b>Note :</b> Please take a note of your booking password for cancellation purpose in future. </p>
			
			</div>
			
			<button type="button" class="btn btn-default" data-dismiss="modal" id="cancel_final">Close</button>
		</div>
	</div>
	
	  <div id="popup2" class="modal fade" role="dialog">
		<div class="dialog">
			<h5 id="txtHint">Details of the booking</h5>
			<div class="details2">
			<div id="status"></div>
			<div id="conf_book_name2"><span id="conf_book_label2">Booking Name : </span><span id="conf_user_name2"></span></div>
		<div id="conf_dura_time2"><span id="conf_dura_label2">Booked Timings : </span><span id="conf_user_timings"></span></div>
            <p id="cancel_box">Please Enter Your Code To Cancel The Booking : <input type="text" id="cancel-code" size="25"/></p>
			</div>
			<div id="note">
			<hr>
			 <b>Note :</b> A password was given to u at the time of booking. Please use that in order to cancel the booking . 
			</div>
			<div class="popup2_buttons">
			<button type="button" class="btn btn-default" id="cancel_booking">Cancel Booking</button>
			<button type="button" class="btn btn-default" data-dismiss="modal" id="cancel_final_popup2">Close</button>
			</div>
		</div>
	</div>
	
 

   </body>
</html>
<script>
 $(document).ready(function(){
 $(".pof_division").hide();
 $(".eof_division").hide();
 $("#eof-link").click(function(){
 $(".eof_division").slideToggle(1000);
 });
 $("#pof-link").click(function(){
 $(".pof_division").slideToggle(1000);
 });

var detail = <?php echo json_encode($arr); ?>;

var button_id = ['000','030','100','130','200','230','300','330','400','430','500','530','600','630','700','730','800','830','900','930','1000','1030','1100','1130','1200','1230','1300','1330','1400','1430','1500','1530','1600','1630','1700','1730','1800','1830','1900','1930','2000','2030','2100','2130','2200','2230','2300','2330']



for(i=0;i<48;i++){


if(detail[i].indexOf('FREE') != -1){
$("#button"+button_id[i]).css("background-color","#3e9d44");
} else{

$("#button"+button_id[i]).css("background-color","#D20C0C");

} 
if (detail[i].indexOf('HOLD') != -1){
$("#button"+button_id[i]).css("background-color","#555555");

}
//i++;

}

setInterval(function(){
$.ajax({
dataType: "json",
url: "room/test?r=<?=$_GET['r']?>&date=<?=$_GET['date']?>", 
success: function(result){
        //$("#div1").html(result);
		var interval_check = result;
		//console.log(interval_check.length);

var button_id = ['000','030','100','130','200','230','300','330','400','430','500','530','600','630','700','730','800','830','900','930','1000','1030','1100','1130','1200','1230','1300','1330','1400','1430','1500','1530','1600','1630','1700','1730','1800','1830','1900','1930','2000','2030','2100','2130','2200','2230','2300','2330']
		


		for(i=0;i<48;i++){
		//console.log(interval_check[i]);
			if(interval_check[i].indexOf('FREE') != -1){
			$("#button"+button_id[i]).css("background-color","#3e9d44");
			
			} else{

			$("#button"+button_id[i]).css("background-color","#D20C0C");
         
			} 
			if (interval_check[i].indexOf('HOLD') != -1){
			$("#button"+button_id[i]).css("background-color","#555555");
			
			
			}
			
			
			}
		

}
}); 

},100);



 
var b = $("#room-value").val();

$("#device_main_img").attr("src",'../assets/css/b'+b+'.jpg');

});


</script>