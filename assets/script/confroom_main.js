var selectedTiming;
var rn;
var device ;
var idd;
var available_count;

$(document).ready(function(){

		
		
$('.room_header').fadeIn(5000);

var month2 = ["1","2","3","4","5","6","7","8","9","10","11","12"]
var DNow = new Date();
var strrr = DNow.getFullYear();
var year_2digits = strrr.toString().substr(2,2);
var date_check = DNow.getDate()+ '/' + month2[DNow.getMonth()]+'/'+year_2digits;



function getUrlVars() {
var vars = {};
var parts = window.location.href.replace(/[?&]+([^=&]+)=([^&]*)/gi, function(m,key,value) {
vars[key] = value;
});
return vars;
}

var date = getUrlVars()["date"]; 

rn= $("#room-value").val();	

setInterval(function(){
var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
		      
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {	
            if(xmlhttp.responseText == "Dont Proceed"){
            
			$('.disabled').css('pointer-events','none');
			$('.disabled').css('opacity','0.5');
            }
            if(xmlhttp.responseText == ""){
            $('.disabled').css('opacity','1.0');
			$('.disabled').css('pointer-events','auto');
             }			 
			//alert(xmlhttp.responseText);
            }
        }
        xmlhttp.open("GET", "../ajax/fbh.php?&rn="+rn+"&date="+date, true);
        xmlhttp.send(); 
},1000);


$('[data-toggle="tooltip"]').tooltip(); 
  
  
$(".bookings").fadeIn(3000);
setInterval(function(){
var dNow = new Date();
var month = ["January","February","March","	April","May","June","July","August","September","October","November","December"]
var localdate= dNow.getDate()  + ' ' + month[dNow.getMonth()] + ' ' + dNow.getFullYear();
var localtime=  dNow.getHours() + ':' + dNow.getMinutes();
var minutes = dNow.getMinutes();
if (minutes < 10){
minutes = '0'+dNow.getMinutes();
localtime=  dNow.getHours() + ':' + minutes;
}



$('#date').text(localdate);
$('#time').text(localtime);


//function to disable when the time is greater than the button time
	$("button.time_button").each(function(i, element){
	
		var initialTime = $(this).val();
		var momentObj = moment(initialTime, ["h:mm A"])
		var convertedTime = momentObj.format("HH:mm");
		var res1 = convertedTime.replace(":", ".");
		var res2 = localtime.replace(":", ".");
	    var currentTime = parseFloat(res2);
		var buttonTime = parseFloat(res1);		
		if(buttonTime < currentTime && $(this).css('background-color')=="rgb(62, 157, 68)" && date_check == date || $(this).css('background-color')=="rgb(85, 85, 85)"){
			$(element).attr('disabled', 'disabled');
		}
		else{
		  $(element).removeAttr('disabled');
		  }
	});
	

	
	

//end of function.
},1);


$("#fbh").click(function(){

					selectedTiming = "13:30f";
					selectedTimingModal = "01:30 PM";
					var selectedval = "13:30f";
				 	$("#room_timing").text(selectedTimingModal); 
					$("#room_timing_val").val(selectedval); 
					$('.duration_select_span').css('display','none');
					$('.fbh_modal_text').css('display','block');
					timer.clearTimeout();
				   timer.countdown( "countdown_timer", 03, 00 );	
			});

			

//initial details on clicking of timing button
$(".time_button").click(function(){
					$(".time_button").css('border', '0');
					//$(this).css('outline', '0px !important');
					$(this).css('border', '2px solid rgba(0, 0, 0, 0.9)');
					$('.book_now').removeAttr('disabled');
					$('.duration_select').prop('selectedIndex',0);
					
					$('#60mins').attr('disabled','disabled');
					
					$('#90mins').attr('disabled','disabled');
					
					$('#120mins').attr('disabled','disabled');
					selectedTiming = $(this).val();
					selectedTimingModal = $(this).text();
					var selectedval = $(this).val();
				 	$("#room_timing").text(selectedTimingModal); 
					
					$("#room_timing_val").val(selectedval); 
			});

$('.book_now').click(function(){
$(".duration_select_span").css('display','block');
$(".fbh_modal_text").css('display','none');	
$('.modal-content').find('input:text').val('');
$('.modal-content').find("input[type='email']").val('');

});		
// validation for dropdown options to appear	
//validations for dropdown of devices.
//getting the device name in hidden input near name field in book device modal
//checking if the values are not empty and enabling book button
//getting the input field values;
//email validation in details and to check whether the fields are not empty
 $("#myModal").on('shown.bs.modal', function () {
 $('#submit_popup').attr('disabled','disabled');
 });
 
setInterval(function(){ 
  $("input").keyup(function(){
  
        if($("#uname").val() != "" && $("#meeting").val() != "" && $("#useremail").val().indexOf(".") != -1 ){
		$('#submit_popup').removeAttr('disabled');
		
		
		}
		else{
		$('#submit_popup').attr('disabled','disabled');
		
		}
    });
},100);	
	


	
//on click of book button in pop up
	$("#submit_popup").click(function(){
	
						$('#popup').removeAttr('disabled');
						var userName = $("#uname").val();
						$("#conf_user_name").text(userName);
						var meetingPurpose = $("#meeting").val();
						$("#conf_user_purp").text(meetingPurpose);
						var startTime = $("#room_timing").text();
						
						//duration calculation at success model
						$("#conf_user_st").text(startTime);
						var opt = $(".duration_select").val();
						var array = ["0:0", "0:30", "01:00", "01:30", "02:00"];
				        
						
					    var dropdown_value = array[opt];
						
						var option = dropdown_value;
						if(opt == 18){
						var option = "09:00";
						}
						//alert(option);
						var totalTime = addTime(startTime,option);
						
						function addTime()
					{
						if (arguments.length < 2)
						{
							if (arguments.length == 1 && isFormattedDate(arguments[0])) return arguments[0];
							else return false;
						}
						var time1Split, time2Split, totalHours, totalMinutes;
						if (isFormattedDate(arguments[0])) var totalTime = arguments[0];
						else return false;
						for (var i = 1; i < arguments.length; i++)
						{
							// Add them up
							time1Split = totalTime.split(':');
							time2Split = arguments[i].split(':');

							totalHours = parseInt(time1Split[0]) + parseInt(time2Split[0]);
							totalMinutes = parseInt(time1Split[1]) + parseInt(time2Split[1]);

							// If total minutes is more than 59, then convert to hours and minutes
							if (totalMinutes > 59)
									{
										totalHours += Math.floor(totalMinutes / 60);
										totalMinutes = totalMinutes % 60;
									}

									totalTime = '0'+totalHours + ':' + padWithZeros(totalMinutes)+' '+'PM';
								}
								
								return totalTime;
							}
							if( totalTime == "010:15 PM" || totalTime == "010:00 PM" || totalTime == "010:30 PM" || totalTime == "011:00 PM" || totalTime == "011:30 PM" || totalTime == "000:00 AM"){
								
								var res = totalTime.slice(1);
								$("#conf_user_et").text(res);
								}
							else{
							$("#conf_user_et").text(totalTime);
							}
							function isFormattedDate(date)
							{
								var splitDate = date.split(':');
								if (splitDate.length == 2 && (parseInt(splitDate[0]) + '').length <= 2 && (parseInt(splitDate[1]) + '').length <= 2) return true;
								else return false;
							}
							function padWithZeros(number)
							{
								var lengthOfNumber = (parseInt(number) + '').length;
								if (lengthOfNumber == 2) return number;
								else if (lengthOfNumber == 1) return '0' + number;
								else if (lengthOfNumber == 0) return '00';
								else return false;
							}
							
								//rest of the details in model
								var totalMembers = $("#meetpurp").val();
								$("#conf_mem_details").text(totalMembers);
							         if ( $("#check").prop("checked") == true){
									 $("#conf_user_projneed").text("Used");
									 } 
									else {
									 $("#conf_user_projneed").text("Not used");
									 }
								
								var userEmail = $("#useremail").val();
								var predefinedText = $(".atces").text();
								var password = Math.floor((Math.random() * 100000) + 9999);
								$("#conf_user_email").text(userEmail + predefinedText);
								//console.log(totalTime);
								
									
							  });
							  
							  
							//timer on booking model
							var timer = {
								timerOut: {},
								countdown: function ( elementName, minutes, seconds )
								{
									var element, endTime, hours, mins, msLeft, time;

									function twoDigits( n )
									{
										return (n <= 9 ? "0" + n : n);
									}

									function updateTimer()
									{
										msLeft = endTime - (+new Date);
										if ( msLeft < 1000 ) {
											element.innerHTML = "00 : 00";
										} else {
											time = new Date( msLeft );
											hours = time.getUTCHours();
											mins = time.getUTCMinutes();
											element.innerHTML = ("0"+(hours ? hours + ':'  + twoDigits( mins ): mins)) +' '+ ':' +' '+  twoDigits( time.getUTCSeconds() );
											timer.timerOut = setTimeout( updateTimer, time.getUTCMilliseconds() + 500 );
										}
										
									}

									element = document.getElementById( elementName );
									endTime = (+new Date) + 1000 * (60*minutes + seconds) + 500;
									updateTimer();
								},
								clearTimeout : function(){
									clearTimeout(timer.timerOut);
								}
							};
							$(".book_now").click(function(){
								timer.clearTimeout();
								timer.countdown( "countdown_timer", 03, 00 );	
							});
//end of calculation of timer , success modal msgs ,all the other details
	
/* $("#myModal").modal({
  backdrop: 'static',
  show:true
  }); */
  
 
$("#fbh").click(function(){
back = true;

 var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function(e) {
		 
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {		
			
            }
        }
        xmlhttp.open("GET", "../ajax/hold.php?st=" + selectedTiming+"&rn="+rn+"&date="+date, true);
        xmlhttp.send(); 

e.preventDefault();	
$('#myModal').modal({
show:true,
backdrop: 'static',
})
});


$(".book_now").click(function(){
back = true;

//alert(rn);
//alert(selectedTiming);
 var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function(e) {
		 
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {		
              //document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
			  available_count = xmlhttp.responseText;
			  //alert(available_count);
			  var arr = ["60mins", "90mins", "120mins"];
			  for(i=0; i< available_count; i++){
			  //alert(arr);
			  $('#'+arr[i]).removeAttr('disabled','disabled');
			  }
			  
            }
        }
        xmlhttp.open("GET", "../ajax/hold.php?st=" + selectedTiming+"&rn="+rn+"&date="+date, true);
        xmlhttp.send(); 

e.preventDefault();	
$('#myModal').modal({
show:true,
backdrop: 'static',
})

});

$(".use_a_device").click(function(){
$('[data-toggle="tooltip"]').tooltip(); 
e.preventDefault();	
$('#devices').modal({
show:true,
backdrop: 'static',
})
});



// $("#cancel_final").click(function(){
// location.reload();
// });
setInterval(function(){
$(".time_button").click(function(){
var x = $(this).css("background-color");
if(x == "rgb(210, 12, 12)" || x == "rgb(85, 85, 85)" ){
$(this).css('border','none !important');
$(".book_now").attr('disabled','disabled');

}
else{
$(".book_now").removeAttr('disabled','disabled');

}
});	

$("#meetpurp").keydown(function(event) { 
return false;
});
},500);

var back = false;
var count = 0;
var cdt;
rn= $("#room-value").val();	

$('.time_button').click(function() {

var timehint = $(this).val();
if ($(this).css('background-color')=="rgb(210, 12, 12)"){
rn= $("#room-value").val();	
    $('#popup2').modal();
	var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {		
              //document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
			 //alert(xmlhttp.responseText);
			  var json = xmlhttp.responseText;
			  var arr = $.parseJSON(json);
			  //alert(xmlhttp.responseText);
			  $("#conf_user_name2").text(arr.a);
			  $("#conf_user_timings").text(arr.b); 
			  $("#status").text(arr.c);
			  
			  if(arr.c == "APPROVAL PENDING"){
			  $("#status").css('color','orange');
			  $('#cancel_box').css('color','black');
			  $('#cancel_box').css('cursor','default');
			  $('#cancel_booking').removeAttr('disabled','disabled');
			  }
			  if(rn.indexOf("room") != -1){
				var status = "APPROVED";
				$("#status").text(status);
				$("#status").css('color','green');
				}
			  if(arr.c == "APPROVED DEVICE"){
			  $("#status").css('color','green');
			  $('#cancel_box').attr('disabled','disabled');
			  $('#cancel_box').css('color','grey');
			  $('#cancel_box').css('cursor','no-drop');
			  $('#cancel_booking').attr('disabled','disabled');
			  }
			  if(arr.c == "RETURNED DEVICE"){
			  $('#status').text('RECIEVED DEVICE');
			  $("#status").css('color','green');
			  $('#cancel_box').attr('disabled','disabled');
			  $('#cancel_box').css('color','grey');
			  $('#cancel_box').css('cursor','no-drop');
			  $('#cancel_booking').attr('disabled','disabled');
			  }
            }
        }
        xmlhttp.open("GET", "../ajax/details.php?timehint=" + timehint+"&rn="+rn+"&date="+date, true);
        xmlhttp.send();
	}
	
	

var delete_time_val = $(this).val();





//BOOKING CANCEL code ..... cancel .php 
$("#cancel_booking").click(function(){
var input = $("#cancel-code").val();
 if(input==""){ 
alert("Please Enter a Cancel Code");
 } 
 else{
 var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {		
              //document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
			 
			 //$("#popup2").hide();
			 alert(xmlhttp.responseText);
			 $('#popup2').modal('hide');
			  //location.reload();
            }
        }
        xmlhttp.open("GET", "../ajax/cancel.php?input=" + input+"&dtv="+delete_time_val+"&rn="+rn+"&date="+date, true);
        xmlhttp.send();
 }
 e.preventDefault();	

 });
});

setInterval(function(){
cdt = $("#countdown_timer").text();
console.log(cdt);
 if( cdt == "00 : 00"){
 count= 1;
 if(count==1){
  //alert("hi");
 var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {		
              //document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
			  $('#myModal').modal('hide');
			  $("#countdown_timer").text("03 : 00");
			  
			  back = false;
			  
			  alert(xmlhttp.responseText);
			  
			  //timer.clearTimeout();
			  //location.reload();
            }
        }
        xmlhttp.open("GET", "../ajax/close.php?st=" + selectedTiming +"&rn="+rn+"&cc="+available_count+"&date="+date, true);
        xmlhttp.send();
					
}
 e.preventDefault();	
}
else{
count=0;
}
},2000);



 //BOOKING Code
$("body").on('click', '#submit_popup', function(){

var st = $( "#room_timing_val" ).val();

var d = $(".duration_select").val();
var n = $("#uname").val();
var rn =$("#room_id").val();



var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {		
              //document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
			  //var text= "03 : 00";
			  
			  $("#countdown_timer").text("03 : 00");
			  timer.clearTimeout();
			  if(xmlhttp.responseText == "OOPS ! Someone else has booked your timings , Please try again !"){
			  alert(xmlhttp.responseText);
			   $('#popup').modal('toggle');
			  }
			  else{
			  var password = xmlhttp.responseText;
			  $("#booking_password_text").text(password);
			  }
            }
        }
        xmlhttp.open("GET", "../ajax/book.php?st=" + st+"&d="+d+"&n="+n+"&rn="+rn+"&cc="+available_count+"&date="+date, true);
        xmlhttp.send();
       
});




//on clicking of close ( / \ ) button in modal 

/* $(".time_button").click(function(){ */
$(".close").click(function(){
 var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {		
              //document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
			  //alert(xmlhttp.responseText);
			   $("#countdown_timer").text("03 : 00");
			   timer.clearTimeout();
			   //alert(xmlhttp.responseText);
            }
        }
        xmlhttp.open("GET", "../ajax/close.php?st=" + selectedTiming +"&rn="+rn+"&cc="+available_count+"&date="+date, true);
        xmlhttp.send();
		 e.preventDefault();	
});
/* }); */


//on clicking of | close | button in modal 

/* $(".time_button").click(function(){ */
$("#submit_close").click(function(){

 var xmlhttp = new XMLHttpRequest();
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {		
              //document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
			  //alert(xmlhttp.responseText);
			  $("#countdown_timer").text("03 : 00");
			  timer.clearTimeout();

            }
        }
        xmlhttp.open("GET", "../ajax/close.php?st=" + selectedTiming +"&rn="+rn+"&cc="+available_count+"&date="+date, true);
        xmlhttp.send();
		 e.preventDefault();	
});
/* }); */




//timer count-down modal close and value refresh ! 











 window.onbeforeunload = function myFunction() {
if(back==true){
console.log(available_count);
var xmlhttp = new XMLHttpRequest();
	 
        xmlhttp.onreadystatechange = function() {
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200) {		
              //document.getElementById("txtHint").innerHTML = xmlhttp.responseText;
			 //alert(xmlhttp.responseText);
			 $("#countdown_timer").text("03 : 00");
            }
        }
		count++;
        xmlhttp.open("GET", "../ajax/reload.php?st=" + selectedTiming +"&rn="+rn+"&cc="+available_count+"&date="+date, true);
        xmlhttp.send();
		return "You tried to navigate from the page. Your bookings may be lost , Please Try Again";
		
}
}



// cancelling booking when the user reloads the page after opening booking modal 

/* var countt = 0;
$("#myModal").on('shown.bs.modal', function () {

jQuery(window).bind('beforeunload', function(e) {

 {
     
} 
	
});		

}); */
//END 

var check = rn.indexOf("room") != -1;

if(check){
$("#myCarousel").css('display','block');
}
else{
$("#myCarousel").hide();
$("#device_main_img").css('display','block');
}



});
